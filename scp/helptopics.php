<?php
/*********************************************************************
    helptopics.php

    Help Topics.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('admin.inc.php');
//include ('../notifications/notification.php');
include_once(INCLUDE_DIR.'class.topic.php');
include_once(INCLUDE_DIR.'class.faq.php');
require_once(INCLUDE_DIR.'class.dynamic_forms.php');

$topic=null;
if($_REQUEST['id'] && !($topic=Topic::lookup($_REQUEST['id'])))
    $errors['err']=sprintf(__('%s: Unknown or invalid ID.'), __('help topic'));

if($_POST){
    switch(strtolower($_POST['do'])){
        case 'update':
            $result = db_query('SELECT isactive, dept_id, topic_pid from ost_help_topic where topic_id = '.$_POST['id'],false);
            $row = db_fetch_row($result);
//            if($_POST['isactive']!=$row[0]){
//                 if($_POST['isactive']==0){
//                     //campagna Chuisa
//                     //sendNotification($_POST['topic'],$_POST['isactive']);
//                     $value = getKpi($row[1],$row[2]);
//                     for($i=1;$i<sizeOf($value);$i++){
//                         $bugPrec = $value[$i-1][5];
//                         if($value[$i][2]==$_POST['topic']){
//                             $esito = $value[$i][5] - $bugPrec;
//                             if($value[$i][5] == 0){
//                                 sendNotificationKpi("Non sono state riscontrate anomalie sulla versione in campo",$_POST['isactive'],1);
//                             }
//                             if($esito < 0 ){
//                                 sendNotificationKpi("La versione in capo ha rilevato dei bug ma meno della versione precedente",$_POST['isactive'],2);
//                             }
//                             if($esito >= 0){
//                                 sendNotificationKpi("La versione in capo ha rilevato piu bug della versione precedente",$_POST['isactive'],3);
//                             }
//                             break;
//                         }
//                     }
//                 }else{
//                     //campagna Aperta
//                     //sendNotification($_POST['topic'],$_POST['isactive']);
//                     $value = getKpi($row[1],$row[2]);
//                 }
//            }
            if(!$topic){
                $errors['err']=sprintf(__('%s: Unknown or invalid'), __('help topic'));
            }elseif($topic->update($_POST,$errors)){
                $sql = "UPDATE ost_help_topic SET created = '".$_POST['created']."', updated = '".$_POST['updated']."' WHERE topic_id = ".$_POST['id'];
                db_query($sql);
                $msg=sprintf(__('Successfully updated %s.'),
                    __('this help topic'));
            }elseif(!$errors['err']){
                $errors['err'] = sprintf('%s %s',
                    sprintf(__('Unable to update %s.'), __('this help topic')),
                    __('Correct any errors below and try again.'));
            }
            break;
        case 'create':
            $_topic = Topic::create();
            if ($_topic->update($_POST, $errors)) {
                $topic = $_topic;
                $msg=sprintf(__('Successfully added %s.'), Format::htmlchars($_POST['topic']));
                $_REQUEST['a']=null;
            }elseif(!$errors['err']){
                $errors['err']=sprintf('%s %s',
                    sprintf(__('Unable to add %s.'), __('this help topic')),
                    __('Correct any errors below and try again.'));
            }
            break;
        case 'mass_process':
            switch(strtolower($_POST['a'])) {
            case 'sort':
                // Pass
                break;
            default:
                if(!$_POST['ids'] || !is_array($_POST['ids']) || !count($_POST['ids']))
                    $errors['err'] = sprintf(__('You must select at least %s.'),
                        __('one help topic'));
            }
            if (!$errors) {
                $count=count($_POST['ids']);

                switch(strtolower($_POST['a'])) {
                    case 'enable':
                        $num = Topic::objects()->filter(array(
                            'topic_id__in' => $_POST['ids'],
                        ))->update(array(
                            'isactive' => true,
                        ));

                        if ($num > 0) {
                            if($num==$count)
                                $msg = sprintf(__('Successfully enabled %s'),
                                    _N(__('selected help topic'), __('selected help topics'), $count));
                            else
                                $warn = sprintf(__('%1$d of %2$d %3$s enabled'), $num, $count,
                                    _N(__('selected help topic'), __('selected help topics'), $count));
                        } else {
                            $errors['err'] = sprintf(__('Unable to enable %s'),
                                _N(__('selected help topic'), __('selected help topics'), $count));
                        }
                        break;
                    case 'disable':
                        $num = Topic::objects()->filter(array(
                            'topic_id__in'=>$_POST['ids'],
                        ))->exclude(array(
                            'topic_id'=>$cfg->getDefaultTopicId(),
                        ))->update(array(
                            'isactive' => false,
                        ));
                        if ($num > 0) {
                            if($num==$count)
                                $msg = sprintf(__('Successfully disabled %s'),
                                    _N(__('selected help topic'), __('selected help topics'), $count));
                            else
                                $warn = sprintf(__('%1$d of %2$d %3$s disabled'), $num, $count,
                                    _N(__('selected help topic'), __('selected help topics'), $count));
                        } else {
                            $errors['err'] = sprintf(__('Unable to disable %s'),
                                _N(__('selected help topic'), __('selected help topics'), $count));
                        }
                        break;
                    case 'delete':
                        $i = Topic::objects()->filter(array(
                            'topic_id__in'=>$_POST['ids']
                        ))->delete();

                        if($i && $i==$count)
                            $msg = sprintf(__('Successfully deleted %s.'),
                                _N(__('selected help topic'), __('selected help topics'), $count));
                        elseif($i>0)
                            $warn = sprintf(__('%1$d of %2$d %3$s deleted'), $i, $count,
                                _N(__('selected help topic'), __('selected help topics'), $count));
                        elseif(!$errors['err'])
                            $errors['err']  = sprintf(__('Unable to delete %s.'),
                                _N(__('selected help topic'), __('selected help topics'), $count));

                        break;
                    case 'sort':
                        try {
                            $cfg->setTopicSortMode($_POST['help_topic_sort_mode']);
                            if ($cfg->getTopicSortMode() == 'm') {
                                foreach ($_POST as $k=>$v) {
                                    if (strpos($k, 'sort-') === 0
                                            && is_numeric($v)
                                            && ($t = Topic::lookup(substr($k, 5))))
                                        $t->setSortOrder($v);
                                }
                            }
                            $msg = __('Successfully set sorting configuration');
                        }
                        catch (Exception $ex) {
                            $errors['err'] = __('Unable to set sorting mode');
                        }
                        break;
                    default:
                        $errors['err']=sprintf('%s - %s', __('Unknown action'), __('Get technical help!'));
                }
            }
            break;
        default:
            $errors['err']=__('Unknown action');
            break;
    }
    if ($id or $topic) {
        if (!$id) $id=$topic->getId();
    }
}

$page='helptopics.inc.php';
$tip_namespace = 'manage.helptopic';
if($topic || ($_REQUEST['a'] && !strcasecmp($_REQUEST['a'],'add'))) {
    $page='helptopic.inc.php';
}

$nav->setTabActive('manage');
$ost->addExtraHeader('<meta name="tip-namespace" content="' . $tip_namespace . '" />',
    "$('#content').data('tipNamespace', '".$tip_namespace."');");
require(STAFFINC_DIR.'header.inc.php');
require(STAFFINC_DIR.$page);
include(STAFFINC_DIR.'footer.inc.php');

function getKpi($dep_id, $topic_pid){
    
    if($topic_pid=="40"){
        $sql = 'SELECT ht.topic_id, ht.status_id, ht.topic, ht.created, ht.updated, ht.isactive, ht.topic_pid  FROM ost_help_topic as ht where dept_id = '.$dep_id.' AND  ht.topic_pid = "40" order by sort';
    } 
    
    
    if($topic_pid=="28"){
        $sql = 'SELECT ht.topic_id, ht.status_id, ht.topic, ht.created, ht.updated, ht.isactive, ht.topic_pid  FROM ost_help_topic as ht where dept_id = '.$dep_id.' AND  ht.topic_pid = "28" order by sort';
    }
    
    
    
    if($topic_pid=="41"){
        $sql = 'SELECT ht.topic_id, ht.status_id, ht.topic, ht.created, ht.updated, ht.isactive, ht.topic_pid  FROM ost_help_topic as ht where dept_id = '.$dep_id.' AND  ht.topic_pid = "41" order by sort';
    }


    
    
    
    
    
    
    $result = db_query($sql);
    
    $help_topics=[];
    $i = 0;
    while ($row = $result->fetch_row()) {
        
        $query_count = 'SELECT count(*) FROM ost_ticket where topic_id ='.$row[0].' and dept_id ='.$dep_id;
        
        $ticket_detail ='select a.ticket_id, c.value, b.created from ost_ticket as a
                    left join ost_form_entry as b on a.ticket_id = b.object_id
                    left join ost_form_entry_values as c on c.entry_id = b.id
                    where topic_id ='.$row[0].'  and b.form_id > 2';
        
        
        $result_ticket = db_query($ticket_detail);
        
        $tickets=[];
        $a = 0;
        $k=2;
        while ($row_ticket = $result_ticket->fetch_row()) {
            
            
            if ($a==0){
                $tickets[$a] = $row_ticket;
                $a++;
            }else{
                if($tickets[$a-1][0]==$row_ticket[0]){
                    $tickets[$a-1][$k+1]=$row_ticket[1];
                    $k++;
                }else{
                    $tickets[$a] = $row_ticket;
                    $a++;
                    $k=2;
                }
                
            }
            
            
            
            
        }
        
        
        $result_count = db_fetch_row(db_query($query_count));
        $active = $row[5];
        $padre = $row[6];
        $row['5']=$result_count[0];
        $row['6']=$tickets;
        $row['7']=$active;
        $row['8']=$padre;
        $help_topics[$i] = $row;
        
        $i++;
    }
    
    return $help_topics;
    
}
?>
