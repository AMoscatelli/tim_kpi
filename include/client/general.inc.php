<?php
require('secure.inc.php');
if(!is_object($thisclient) || !$thisclient->isValid()) die('Access denied'); //Double check again.
$sql ='SELECT * FROM ost_department';
$result = db_query($sql);
//$row = db_fetch_array($result);
$department_name=[];
$dep_id=[];
$dep_image=[];
$i = 0;
while ($row = $result->fetch_row()) {
    
    $department_name[$i] = $row[8];
    $dep_id[$i] = $row[0];
    $dep_image[$i] = $row[9];
    $i++;
 
}
?>
<!-- <h1><?php //echo __('Campagne ALTRAN');?></h1>
<p><?php //echo __('Scegli un area per vsualizzare le relative Campagne');?></p> -->

<!-- Projects section v.1 -->
<section class="text-center">

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold">Campagne ALTRAN</h2>
  <!-- Section description -->
  <p class="grey-text w-responsive mx-auto mb-5">Scegli un area per visualizzare le relative Campagne</p>

    <!-- Grid row -->
  <div class="row text-center" id="department0"></div>
    <!-- /Grid row -->
    
    <!-- Grid row -->
  <div class="row text-center mt-2" id="department1"></div>
    <!-- /Grid row -->
    
    <!-- Grid row -->
  <div class="row text-center mt-2" id="department2"></div>
    <!-- /Grid row -->
    
    <!-- Grid row -->
  <div class="row text-center mt-2" id="department3"></div>
    <!-- /Grid row -->


<div id="modal2"></div>
</section>
<!-- Projects section v.1 -->

<script type="text/javascript">
//
//Gestione del Browser per permettere la visualizzazione su Explorer
var sAgent = window.navigator.userAgent;
var Idx = sAgent.indexOf("MSIE");

// If IE, return version number.
if (Idx > 0){
	//explorer version
	var department_name = <?php echo json_encode($department_name);?>;
	var dep_id = <?php echo json_encode($dep_id);?>;
	var dep_image = <?php echo json_encode($dep_image);?>;

	for(var i = 0; i < Object.keys(department_name).length; i++){

	    var html = '<!-- Grid column -->';
	    html+='<div class="col-sm-2">';
	    html+='<!--Featured image-->';
	    html+='<a href="campagnaDetails.php?dep_id='+dep_id[i]+'&dep_name='+department_name[i]+'&addBack=true">';
	    html+='<div class="">';
	    html+='<img src="<?php echo ASSETS_PATH."images/"?>'+dep_image[i]+'"  height="100%" width="100%" class="box" alt="Sample project image">';
	    html+='<div class=""></div>';
	    html+='</a>';
	    html+='</div>';
	    html+='<!--Excerpt-->';
	    html+='<div class="mt-2">';
	    html+='<h6 class="font-weight-bold">'+department_name[i]+'</h6>';
	    html+='<p class="grey-text">';
	    html+='</p>';
	    //html+='<a class="btn btn-indigo btn-sm" href="campagnaDetails.php?dep_id='+dep_id[i]+'"><i class="fa fa-clone left"></i> Mostra Campagne</a>';
	    html+='</div>';
	    html+='</div>';
	    html+='<!-- Grid column -->';
	    
	    if(i<6){
	    	$('#department0').append(html);
	    }else if(i<12){
	    	$('#department1').append(html);
	    }else if (i<18){
	    	$('#department2').append(html);
	    }else if (i<24){
	    	$('#department3').append(html);
	    }
	}
//	  document.write("<link rel=\"stylesheet\" href=\"css/loginIE.css\" type=\"text/css\" />");

// If IE 11 then look for Updated user agent string.
}else if (!!navigator.userAgent.match(/Trident\/7\./)){ 
//	  document.write("<link rel=\"stylesheet\" href=\"css/loginIE.css\" type=\"text/css\" />");
//explorer version
	var department_name = <?php echo json_encode($department_name);?>;
	var dep_id = <?php echo json_encode($dep_id);?>;
	var dep_image = <?php echo json_encode($dep_image);?>;

	for(var i = 0; i < Object.keys(department_name).length; i++){

	    var html = '<!-- Grid column -->';
	    html+='<div class="col-sm-2">';
	    html+='<!--Featured image-->';
	    html+='<a href="campagnaDetails.php?dep_id='+dep_id[i]+'&dep_name='+department_name[i]+'&addBack=true">';
	    html+='<div class="">';
	    html+='<img src="<?php echo ASSETS_PATH."images/"?>'+dep_image[i]+'"  height="100%" width="100%" class="box" alt="Sample project image">';
	    html+='<div class=""></div>';
	    html+='</a>';
	    html+='</div>';
	    html+='<!--Excerpt-->';
	    html+='<div class="mt-2">';
	    html+='<h6 class="font-weight-bold">'+department_name[i]+'</h6>';
	    html+='<p class="grey-text">';
	    html+='</p>';
	    //html+='<a class="btn btn-indigo btn-sm" href="campagnaDetails.php?dep_id='+dep_id[i]+'"><i class="fa fa-clone left"></i> Mostra Campagne</a>';
	    html+='</div>';
	    html+='</div>';
	    html+='<!-- Grid column -->';
	    
	    if(i<6){
	    	$('#department0').append(html);
	    }else if(i<12){
	    	$('#department1').append(html);
	    }else if (i<18){
	    	$('#department2').append(html);
	    }else if (i<24){
	    	$('#department3').append(html);
	    }
	}

}else{
	//other browser 
	var department_name = <?php echo json_encode($department_name);?>;
	var dep_id = <?php echo json_encode($dep_id);?>;
	var dep_image = <?php echo json_encode($dep_image);?>;

	for(var i = 0; i < Object.keys(department_name).length; i++){

	    var html = '<!-- Grid column -->';
	    html+='<div class="col-lg-2 col-md-12 mb-lg-0 mb-4">';
	    html+='<!--Featured image-->';
	    html+='<a href="campagnaDetails.php?dep_id='+dep_id[i]+'&dep_name='+department_name[i]+'&addBack=true">';
	    html+='<div class="view overlay rounded z-depth-1">';
	    html+='<img src="<?php echo ASSETS_PATH."images/"?>'+dep_image[i]+'" class="img-fluid" alt="Sample project image">';
	    html+='<div class="mask rgba-white-slight"></div>';
	    html+='</a>';
	    html+='</div>';
	    html+='<!--Excerpt-->';
	    html+='<div class="card-body pb-0">';
	    html+='<h6 class="font-weight-bold my-3">'+department_name[i]+'</h6>';
	    html+='<p class="grey-text">';
	    html+='</p>';
	    //html+='<a class="btn btn-indigo btn-sm" href="campagnaDetails.php?dep_id='+dep_id[i]+'"><i class="fa fa-clone left"></i> Mostra Campagne</a>';
	    html+='</div>';
	    html+='</div>';
	    html+='<!-- Grid column -->';
	    
	    if(i<6){
	    	$('#department0').append(html);
	    }else if(i<12){
	    	$('#department1').append(html);
	    }else if (i<18){
	    	$('#department2').append(html);
	    }else if (i<24){
	    	$('#department3').append(html);
	    }
	}
	
}
	  //document.write("<link rel=\"stylesheet\" href=\"css/login.css\" type=\"text/css\" />");





function lanciaModal(){
	$('#centralModalSm').modal();
}
</script>