        </div>
    </div>
    <div id="footer">
        <p>Copyright &copy; <?php echo date('Y'); ?> <?php echo (string) $ost->company ?: 'osTicket.com'; ?> - <?php echo __('All rights reserved.'); ?></p>
        <span>
        <!-- <a id="poweredBy" href="http://www.altran.com/it/it/" target="_blank" alt="<?php echo __('Helpdesk software - powered by Altran'); ?>"></a> -->
		<a id="providedBy" href="https://www.altran.com/it/it/industries/telecom-media/" target="_blank" alt="<?php echo __('Helpdesk software - provided by SMILELAB'); ?>"></a>
        
        </span>
    </div>
<div id="overlay_loading"></div>

        <div class="load-wrapper" id="loader-8">
            <div id="loader"></div>
            <div id="loader"></div>
            <div id="loader"></div>
            <div id="loader"></div>
    		<div id="loader"></div>
            <div id="loader"></div>
    	</div>
<?php
if (($lang = Internationalization::getCurrentLanguage()) && $lang != 'en_US') { ?>
    <script type="text/javascript" src="ajax.php/i18n/<?php
        echo $lang; ?>/js"></script>
<?php } ?>
<script type="text/javascript">
    getConfig().resolve(<?php
        include INCLUDE_DIR . 'ajax.config.php';
        $api = new ConfigAjaxAPI();
        print $api->client(false);
    ?>);
</script>
</body>
<!-- JQuery -->
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>custom/js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>custom/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>custom/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>custom/js/mdb.min.js"></script>

<script type="text/javascript">

//Inizializzo MDBootstrap 
new WOW().init();


</script>
<!-- Init Firebase -->
<script>
initFirebase("<?php echo $server_public_key; ?>","<?php echo $topic_name; ?>");
</script>
</html>
