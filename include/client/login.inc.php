<?php
if(!defined('OSTCLIENTINC')) die('Access Denied');

$email=Format::input($_POST['luser']?:$_GET['e']);
$passwd=Format::input($_POST['lpasswd']?:$_GET['t']);

$content = Page::lookupByType('banner-client');

//if ($content) {
  //  list($title, $body) = $ost->replaceTemplateVariables(array($content->getName(), $content->getBody()));
//} else {
    $title = __('Sign In');
    $body = __('Benvenuto, accedi per poter aggiugere un nuovo BUG');
//}

?>
<!-- <h1><?php echo $title; ?></h1>
<p><?php echo $body; ?></p> -->

<div id="brickwall"></div>
<div id="loginBox">
    <div id="blur">
        <div id="background"></div>
    </div>
    <h1 id="logo"><a href="index.php">
        <span class="valign-helper"></span>
        <img src="logo.php?login" alt="Altran :: <?php echo __('Staff Control Panel');?>" id="altran"/><img src="<?php echo ASSETS_PATH."images/"?>tim.png" id="tim" width="10%">
    </a></h1>
    <h3><?php echo Format::htmlchars($msg); ?></h3>
    <div class="banner center-text"><small>Benvenuto nel sistema di gestione KPI</small></div>

<form action="login.php" method="post">
    <?php csrf_token(); ?>
<div style="display:table-row">
    <div>
    <strong><?php echo Format::htmlchars($errors['login']); ?></strong>
    <div>
        <input id="username" placeholder="<?php echo __('Email or Username'); ?>" type="text" name="luser" size="30" value="<?php echo $email; ?>" class="nowarn">
    </div>
    <div>
        <input id="passwd" placeholder="<?php echo __('Password'); ?>" type="password" name="lpasswd" size="30" value="<?php echo $passwd; ?>" class="nowarn"></td>
    </div>
    <p>
        <input class="btn" type="submit" value="<?php echo __('Sign In'); ?>">
        <input class="btn" type="submit" value="gestione kpi" name="kpi">
<?php if ($suggest_pwreset) { ?>
        <a style="padding-top:4px;display:inline-block;" href="pwreset.php"><?php echo __('Forgot My Password'); ?></a>
<?php } ?>
    </p>
    </div>
    <div style="display:table-cell;padding: 15px;vertical-align:top">
<?php

$ext_bks = array();
foreach (UserAuthenticationBackend::allRegistered() as $bk)
    if ($bk instanceof ExternalAuthentication)
        $ext_bks[] = $bk;

if (count($ext_bks)) {
    foreach ($ext_bks as $bk) { ?>
<div class="external-auth"><?php $bk->renderExternalLink(); ?></div><?php
    }
}
if ($cfg && $cfg->isClientRegistrationEnabled()) {
    if (count($ext_bks)) echo '<hr style="width:70%"/>'; ?>
    <!-- <div style="margin-bottom: 5px">
    <?php //echo __('Not yet registered?'); ?> <a href="account.php?do=create"><?php //echo __('Create an account'); ?></a>
    </div>-->
<?php } ?>
    <div>
    <!-- parte che indica "Accedi come operatore" -->
    <!-- <b><?php echo __("I'm an agent"); ?></b> — -->
    <!-- <a href="<?php echo ROOT_PATH; ?>scp/"><?php echo __('sign in here'); ?></a> -->
    </div>
    </div>
</div>
</form>


<br>
<p>
<?php
if ($cfg->getClientRegistrationMode() != 'disabled'
    || !$cfg->isClientLoginRequired()) {
    //echo sprintf(__('If this is your first time contacting us or you\'ve lost the ticket number, please %s open a new ticket %s'),
    //    '<a href="open.php">', '</a>');
} ?>
</p>

    <div id="company">
        <div class="content">
            <?php echo __('Copyright'); ?> &copy; <?php echo Format::htmlchars($ost->company) ?: date('Y'); ?>
        </div>
    </div>
</div>