<?php
require('secure.inc.php');
if(!is_object($thisclient) || !$thisclient->isValid()) die('Access denied'); //Double check again.

if(isset($_GET['camp_id'])){
    $camp_id = $_GET['camp_id'];
    $camp_name = $_GET['camp_name'];
    $dep_name = $_GET['dep_name'];
}else{
    Http::redirect('index.php');
}
//SELECT ht.topic_id, ht.status_id, ht.topic, ht.created, ht.updated  FROM ost_help_topic as ht where dept_id = 5 order by sort
// $sql ='select a.ticket_id, c.value, b.created from ost_ticket as a
// join ost_form_entry as b on a.ticket_id = b.object_id
// join ost_form_entry_values as c on c.entry_id = b.id
// where topic_id ='.$camp_id.'  and b.form_id > 2';


$sql ='select a.ticket_id, c.value, a.created from ost_ticket as a
join ost_form_entry as b on a.ticket_id = b.object_id
join ost_form_entry_values as c on c.entry_id = b.id
where topic_id ='.$camp_id.'  and b.form_id > 2  order by b.created desc';

//$sql = 'SELECT ht.topic_id, ht.status_id, ht.topic, ht.created, ht.updated  FROM ost_help_topic as ht where dept_id = '.$dep_id.' order by sort';
$result = db_query($sql);



$tickets=[];
$i = 0;
$k=2;
while ($row_ticket = $result->fetch_row()) {
    
    
    if ($i==0){
        $tickets[$i] = $row_ticket;
        $i++;
    }else{
        if($tickets[$i-1][0]==$row_ticket[0]){
            $tickets[$i-1][$k+1]=$row_ticket[1];
            $k++;
        }else{
            $tickets[$i] = $row_ticket;
            $i++;
            $k=2;
        }
        
    }
    
 
    
}

//}
?>
<!-- <h1><?php //echo __('Campagne ALTRAN');?></h1>
<p><?php //echo                   __('Scegli un area per vsualizzare le relative Campagne');?></p> -->

<!-- Projects section v.1 -->
<section class="text-center">

<!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold"><?php echo $dep_name?> - <?php echo $camp_name?></h2>
  <!-- Section description -->
  <div class="card">
    <div class="card-body">

        <!-- Grid row 
        <div class="row">

            <!-- Grid column 
            <div class="col-md-12">

                <div class="input-group md-form form-sm form-2 pl-0 mb-0">
                    <input class="form-control my-0 py-1 grey-border" type="text" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <span class="input-group-text waves-effect grey lighten-3" id="basic-addon1">
                            <a><i class="fa fa-search text-grey" aria-hidden="true"></i></a>
                        </span>
                    </div>
                </div>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!--Table-->
        
         <div class="text-center">
         	  <h4 class="h4-responsive font-weight mt-2">Status Anomalie</h4>
         	<div class="row mb-3">
             	<div class="col-md-3">
            		<span class="min-chart" id="chart-high" data-percent="0"><span class="percent"></span></span>
            		<h5><span id="span-high" class="label red darken-4" style="font-size: large">High <i class="fa fa-arrow-circle-up"></i></span></h5>
        		</div>
        		<div class="col-md-3">
            		<span class="min-chart" id="chart-medium" data-percent="0"><span class="percent"></span></span>
            		<h5><span id="span-medium" class="label amber darken-4" style="font-size: large">Medium<i class="fa fa-arrow-circle-up"></i></span></h5>
        		</div>
        		<div class="col-md-3">
            		<span class="min-chart" id="chart-low" data-percent="0"><span class="percent"></span></span>
            		<h5><span id="span-low" class="label amber" style="font-size: large">Low <i class="fa fa-arrow-circle-up"></i></span></h5>
        		</div>
        		<div class="col-md-3">
            		<span class="min-chart" id="chart-informative" data-percent="0"><span class="percent"></span></span>
            		<h5><span id="span-informative" class="label green" style="font-size: large">Informative <i class="fa fa-arrow-circle-up"></i></span></h5>
        		</div>
    		</div>
    		<hr class="mt-4">
    		<div class="row" style="padding:30px">
    			<!--Table-->
        <table class="table table-striped">

            <!--Table head-->
            <thead>
                <tr>
                    <th class="font-weight-bold" style="font-size:1.1em">TESSA #</th>
                    <th class="font-weight-bold" style="font-size:1.1em"><i  id="icon" onclick="clear1();" class="fa fa-angle-down" aria-hidden="true"></i><i  id="icon_up" onclick="clear1();" class="fa fa-angle-up" aria-hidden="true"style="display: none"></i> Priorit&agrave</th>
                    <th class="font-weight-bold" style="font-size:1.1em"><i  id="icon2" onclick="clear2();" class="fa fa-angle-down" aria-hidden="true"></i><i  id="icon2_up" onclick="clear2();" class="fa fa-angle-up" aria-hidden="true"style="display: none"></i>Data Apertura</th>
                    <th class="font-weight-bold" style="font-size:1.1em">Dettaglio</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody id="campagna">

            </tbody>
            <!--Table body-->
        </table>
        <!--Table-->
    		</div>
    	</div>            
    </div>
</section>

<section class="text-center section-blog-fw mt-5 pb-3 wow fadeIn" id="gridAnomalie">
    		
		</section>
		
<div id="modale"></div>
<!-- Projects section v.1 -->
<script>
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
var order=null;
var order_date=null;
var camp_id=<?php echo $camp_id?>;
var camp_name='<?php echo $camp_name?>';
var dep_name='<?php echo $dep_name?>';

function clear1(){
	
	 $('#campagna').html("");
	 var h=[];
	 var m=[];
	 var l=[];
	 var inf=[];
	 
	 for (var i = 0; i < Object.keys(tickets).length; i++){

		 var temp= JSON.parse(tickets[i][3]);
		    var firstProp;
		    for(var key in temp) {
		        if(temp.hasOwnProperty(key)) {
		            firstProp = temp[key];
		            break;
		       }
		    }

		    if (firstProp=='High') h.push(tickets[i]);
			else if (firstProp=='Medium') m.push(tickets[i]);
			else if (firstProp=='Low')  l.push(tickets[i]);
			else if (firstProp=='Informative') inf.push(tickets[i]);

	 }	    

	 var new_ticke=[];
	 if (order==null || order == 1){
	 h.forEach(function(element){
		 new_ticke.push(element);
		});
	 m.forEach(function(element){
		 new_ticke.push(element);
		});
	 l.forEach(function(element){
		 new_ticke.push(element);
		});
	 inf.forEach(function(element){
		 new_ticke.push(element);
		});
		order =2;
		$('#icon').css('display','inline');
		$('#icon_up').css('display','none');
	 }else{

		 inf.forEach(function(element){
			 new_ticke.push(element);
			});
		 l.forEach(function(element){
			 new_ticke.push(element);
			});
		 m.forEach(function(element){
			 new_ticke.push(element);
			 
			});
		 h.forEach(function(element){
			 new_ticke.push(element);
			 document.getElementById('span-high').innerHTML = h;
			});
		 order =1;
		 $('#icon').css('display','none');
			$('#icon_up').css('display','inline');
			}

	 for (var i = 0; i < Object.keys(new_ticke).length; i++){

		 var temp= JSON.parse(new_ticke[i][3]);
		    var firstProp;
		    for(var key in temp) {
		        if(temp.hasOwnProperty(key)) {
		            firstProp = temp[key];
		            break;
		       }
		    }
	 
	 
		    
		var html = "<!-- riga nascosta per inserimento parametri -->";
		if (firstProp=='High') html += '<tr class="high">';
		else if (firstProp=='Medium') html +=  '<tr class="medium">';
		else if (firstProp=='Low')  html += '<tr class="low">';
		else if (firstProp=='Informative') html +=  '<tr class="informative">';  

		var tessa_name = "'"+new_ticke[i][1]+"'";
		var tessa_data = "'"+moment(new_ticke[i][2].replace(" ","T")).format('DD-MM-YYYY')+"'";   
		var tessa_desc = new_ticke[i][4];
		var tessa_priority = "'"+firstProp+"'";
		var tessa_desc_new='';
		for (var z=0;z<tessa_desc.length;z++){
			if(tessa_desc.charAt(z)=="'"){
				tessa_desc_new+="\\'";
				//z=z+1;
			}else if(tessa_desc.charAt(z)=='"'){
				tessa_desc_new+="\\'\\'";
					//z=z+1;
			}else{
				tessa_desc_new+=tessa_desc.charAt(z);
			}
				
				
			}
		var tessa_desc = "'"+tessa_desc_new+"'";
	    
	    //html += '<th scope="row">'+(i+1)+'</th>';
	    html += '<td class="center font-weight-bold">'+new_ticke[i][1]+'</td>';
	    
	    html += '<td class="center text-uppercase  font-weight-bold">'+firstProp+'</td>';
	    html += '<td class="center font-weight-bold">'+moment(new_ticke[i][2].replace(" ","T")).format('DD-MM-YYYY')+'</td>';
	    html += '<td><i class="center font-weight-bold fa fa-search" aria-hidden="true" data-toggle="tooltip" data-placement="left" title="Mostra Descrizione" style="font-size: large" onclick="doModal('+tessa_name+','+tessa_data+','+tessa_desc+','+tessa_priority+')"></i></td>';
	    html += '</tr>';

	    //data.push(help_topics[i][5]);
	    //labels.push(help_topics[i][2]);
	    
	    $('#campagna').append(html);
	}
}



initCharts('chart-high','#b71c1c');
initCharts('chart-medium','#ff6f00');
initCharts('chart-low','#ffc107');
initCharts('chart-informative','#4caf50');

function initCharts(id, color, data) {
	$(function () {
	    $('.min-chart#'+ id).easyPieChart({
	        barColor: color,
	        onStep: function (from, to, percent) {
	            $(this.el).find('.percent').text(Math.round(percent));
	        }
	    });
	});
		
}


var tickets= <?php echo json_encode($tickets);?>;
var inf=0;
var h=0;
var m=0;
var l=0;
var camp_id=<?php echo $camp_id?>;
var camp_name='<?php echo $camp_name?>';
var dep_name='<?php echo $dep_name?>';
for (var i = 0; i < Object.keys(tickets).length; i++){
	 var temp= JSON.parse(tickets[i][3]);
	    var firstProp;
	    for(var key in temp) {
	        if(temp.hasOwnProperty(key)) {
	            firstProp = temp[key];
	            break;
	       }
	    }
	var html = "<!-- riga nascosta per inserimento parametri -->";
	if (firstProp=='High') html += '<tr class="high">';
	else if (firstProp=='Medium') html +=  '<tr class="medium">';
	else if (firstProp=='Low')  html += '<tr class="low">';
	else if (firstProp=='Informative') html +=  '<tr class="informative">';  
    
    //html += '<th scope="row">'+(i+1)+'</th>';
    html += '<td class="center font-weight-bold">'+tickets[i][1]+'</td>';
   
   
	if (firstProp=='High') h=h+1;
	else if (firstProp=='Medium') m=m+1;
	else if (firstProp=='Low') l=l+1;
	else if (firstProp=='Informative') inf=inf+1;  
	var tessa_name = "'"+tickets[i][1]+"'";
	var tessa_data = "'"+moment(tickets[i][2].replace(" ","T")).format('DD-MM-YYYY')+"'";   
	var tessa_desc = tickets[i][4];
	var tessa_priority = "'"+firstProp+"'";
	var tessa_desc_new='';
	for (var z=0;z<tessa_desc.length;z++){
		if(tessa_desc.charAt(z)=="'"){
			tessa_desc_new+="\\'";
			//z=z+1;
		}else if(tessa_desc.charAt(z)=='"'){
			tessa_desc_new+="\\'\\'";
				//z=z+1;
		}else{
			tessa_desc_new+=tessa_desc.charAt(z);
		}
			
			
		}
	var tessa_desc = "'"+tessa_desc_new+"'";
    html += '<td class="center text-uppercase font-weight-bold">'+firstProp+'</td>';
    html += '<td class="center font-weight-bold">'+moment(tickets[i][2].replace(" ","T")).format('DD-MM-YYYY')+'</td>';
    html += '<td><i class="center font-weight-bold fa fa-search" aria-hidden="true" data-toggle="tooltip" data-placement="left" title="Mostra Descrizione" style="font-size: large" onclick="doModal('+tessa_name+','+tessa_data+','+tessa_desc+','+tessa_priority+')"></i></td>';
    html += '</tr>';

    //data.push(help_topics[i][5]);
    //labels.push(help_topics[i][2]);
    
    $('#campagna').append(html);
}
tot=h+m+l+inf;
$("#chart-high").attr('data-percent', h/tot*100);
$("#chart-medium").attr('data-percent', m/tot*100);
$("#chart-low").attr('data-percent', l/tot*100);    
$("#chart-informative").attr('data-percent', inf/tot*100);

document.getElementById('span-high').innerHTML = "High ("+h+")";
document.getElementById('span-medium').innerHTML = "Medium ("+m+")";
document.getElementById('span-low').innerHTML = "Low ("+l+")";
document.getElementById('span-informative').innerHTML = "Informative ("+inf+")";

//Modal

function doModal(tessa_name,tessa_data,tessa_desc,tessa_priority) {
	//var html = '<div class="modal fade" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
	var modal_type;
	if (tessa_priority=="Informative") modal_type="informative";
	else if (tessa_priority=="High") modal_type="high";
	else if (tessa_priority=="Medium") modal_type="medium";
	else if (tessa_priority=="Low") modal_type="low";
	
	var html =' <!-- Central Modal Medium Info -->';
 	html +=  '<div class="modal fade" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
	html += '<div class="modal-dialog modal-notify modal-'+modal_type+'" role="document">';
	html += '<!--Content-->';
	html += '<div class="modal-content">';
	html += '<!--Header-->';
	html += '<div class="modal-header">';
	html += '<p class="heading lead text-center">TESSA #'+tessa_name+' del '+tessa_data+'</p>';

	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
	html += '<span aria-hidden="true" class="white-text">&times;</span>';
	html += '</button>';
	html += '</div>';

	html += '<!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="text-center">';
	//html += '<i class="fa fa-check fa-4x mb-3 animated rotateIn"></i>';
	html += '<canvas id="gauge" class="200x160px"></canvas>';
	html += '<p>'+tessa_desc+'</p>';
	html += '</div>';
	html += '</div>';

	html += '<!--Footer-->';
	html += '<div class="modal-footer justify-content-center">';
	html += '<a type="button" class="btn btn-outline-'+modal_type+' waves-effect" data-dismiss="modal">CHIUDI</a>';
	html += '</div>';
	html += '</div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!-- Central Modal Medium Info-->';
	
	
	//html += '</div>';
	$("#modale").html(html);
	$('#modalPush').modal('show');
	console.log(camp_id);

	$('#modalPush').on('shown.bs.modal',function(event){

		var opts = {
				  lines: 12,
				  angle: 0.15,
				  lineWidth: 0.44,
				  pointer: {
				    length: 0.5,
				    strokeWidth: 0.035,
				    color: '#000000'
				  },
				  limitMax: 'false', 
				  percentColors: [[0.25, "#4caf50 " ], [0.50, "#ffc107"], [0.75, "#ff6f00"],[1.0, "#b71c1c"]], // !!!!
				  renderTicks: {
			          divisions: 4,
			          divWidth: 0.5,
			          divLength: 0.1,
			          divColor: "#000",
			          }
			    
				  //strokeColor: '#E0E0E0',
				  //generateGradient: true
				};

		
				var target = document.getElementById('gauge'); // your canvas element
				var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
				gauge.maxValue = 4000; // set max gauge value
				gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
				gauge.animationSpeed = 20; // set animation speed (32 is default value)
				var val;
				if (tessa_priority=="Informative") val=1000;
				else if (tessa_priority=="High") val=4000;
				else if (tessa_priority=="Medium") val=3000;
				else if (tessa_priority=="Low") val=2000;
				
				gauge.set(val); // set actual value

		});
}

function clear2(){

	$('#campagna').html("");

	var tickets= <?php echo json_encode($tickets);?>;
	var inf=0;
	var h=0;
	var m=0;
	var l=0;
	var camp_id=<?php echo $camp_id?>;
	var camp_name='<?php echo $camp_name?>';
	var dep_name='<?php echo $dep_name?>';
	
	if(order_date==null|order_date==1) {
	for (var i = 0; i < Object.keys(tickets).length; i++){
		 var temp= JSON.parse(tickets[i][3]);
		    var firstProp;
		    for(var key in temp) {
		        if(temp.hasOwnProperty(key)) {
		            firstProp = temp[key];
		            break;
		       }
		    }
		var html = "<!-- riga nascosta per inserimento parametri -->";
		if (firstProp=='High') html += '<tr class="high">';
		else if (firstProp=='Medium') html +=  '<tr class="medium">';
		else if (firstProp=='Low')  html += '<tr class="low">';
		else if (firstProp=='Informative') html +=  '<tr class="informative">';  
	    
	    //html += '<th scope="row">'+(i+1)+'</th>';
	    html += '<td class="center font-weight-bold">'+tickets[i][1]+'</td>';
	   
	   
		if (firstProp=='High') h=h+1;
		else if (firstProp=='Medium') m=m+1;
		else if (firstProp=='Low') l=l+1;
		else if (firstProp=='Informative') inf=inf+1;  
		var tessa_name = "'"+tickets[i][1]+"'";
		var tessa_data = "'"+moment(tickets[i][2].replace(" ","T")).format('DD-MM-YYYY')+"'";   
		var tessa_desc = tickets[i][4];
		var tessa_priority = "'"+firstProp+"'";
		var tessa_desc_new='';
		for (var z=0;z<tessa_desc.length;z++){
			if(tessa_desc.charAt(z)=="'"){
				tessa_desc_new+="\\'";
				//z=z+1;
			}else if(tessa_desc.charAt(z)=='"'){
				tessa_desc_new+="\\'\\'";
					//z=z+1;
			}else{
				tessa_desc_new+=tessa_desc.charAt(z);
			}
				
				
			}
		var tessa_desc = "'"+tessa_desc_new+"'";
	    html += '<td class="center text-uppercase font-weight-bold">'+firstProp+'</td>';
	    html += '<td class="center font-weight-bold">'+moment(tickets[i][2].replace(" ","T")).format('DD-MM-YYYY')+'</td>';
	    html += '<td><i class="center font-weight-bold fa fa-search" aria-hidden="true" data-toggle="tooltip" data-placement="left" title="Mostra Descrizione" style="font-size: large" onclick="doModal('+tessa_name+','+tessa_data+','+tessa_desc+','+tessa_priority+')"></i></td>';
	    html += '</tr>';

	    //data.push(help_topics[i][5]);
	    //labels.push(help_topics[i][2]);
	    	    $('#icon2').css('display','inline');
			$('#icon2_up').css('display','none');
	    
	    order_date = 2;
	    $('#campagna').append(html);

	} }else {
		for (var i = Object.keys(tickets).length-1; i > 0; i--){
			 var temp= JSON.parse(tickets[i][3]);
			    var firstProp;
			    for(var key in temp) {
			        if(temp.hasOwnProperty(key)) {
			            firstProp = temp[key];
			            break;
			       }
			    }
			var html = "<!-- riga nascosta per inserimento parametri -->";
			if (firstProp=='High') html += '<tr class="high">';
			else if (firstProp=='Medium') html +=  '<tr class="medium">';
			else if (firstProp=='Low')  html += '<tr class="low">';
			else if (firstProp=='Informative') html +=  '<tr class="informative">';  
		    
		    //html += '<th scope="row">'+(i+1)+'</th>';
		    html += '<td class="center font-weight-bold">'+tickets[i][1]+'</td>';
		   
		   
			if (firstProp=='High') h=h+1;
			else if (firstProp=='Medium') m=m+1;
			else if (firstProp=='Low') l=l+1;
			else if (firstProp=='Informative') inf=inf+1;  
			var tessa_name = "'"+tickets[i][1]+"'";
			var tessa_data = "'"+moment(tickets[i][2].replace(" ","T")).format('DD-MM-YYYY')+"'";   
			var tessa_desc = tickets[i][4];
			var tessa_priority = "'"+firstProp+"'";
			var tessa_desc_new='';
			for (var z=0;z<tessa_desc.length;z++){
				if(tessa_desc.charAt(z)=="'"){
					tessa_desc_new+="\\'";
					//z=z+1;
				}else if(tessa_desc.charAt(z)=='"'){
					tessa_desc_new+="\\'\\'";
						//z=z+1;
				}else{
					tessa_desc_new+=tessa_desc.charAt(z);
				}
					
					
				}
			var tessa_desc = "'"+tessa_desc_new+"'";
		    html += '<td class="center text-uppercase font-weight-bold">'+firstProp+'</td>';
		    html += '<td class="center font-weight-bold">'+moment(tickets[i][2].replace(" ","T")).format('DD-MM-YYYY')+'</td>';
		    html += '<td><i class="center font-weight-bold fa fa-search" aria-hidden="true" data-toggle="tooltip" data-placement="left" title="Mostra Descrizione" style="font-size: large" onclick="doModal('+tessa_name+','+tessa_data+','+tessa_desc+','+tessa_priority+')"></i></td>';
		    html += '</tr>';

		    //data.push(help_topics[i][5]);
		    //labels.push(help_topics[i][2]);
		    $('#icon2').css('display','none');
			$('#icon2_up').css('display','inline');
		    
		    order_date = 1;
		    $('#campagna').append(html);


		
	}
	}


}




</script>